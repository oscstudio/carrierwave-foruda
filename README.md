# Carrierwave for Foruda

This gem adds support for [foruda](https://gitee.com/oscstudio/foruda) to [Carrierwave](https://github.com/jnicklas/carrierwave)


## Installation

modify your Gemfile to add:

```ruby
gem "carrierwave-foruda", gitee: "oscstudio/carrierwave-foruda"
```

> Attention:
**carrierwave-foruda** is only a sub-component for **carrierwave**. please install carrierwave at first.
**carrierwave-foruda** Supports both local storage and foruda storage at same time.

## Configuration

```ruby
Carrierwave.configure do |config|
    config.foruda_enabled = true
    config.foruda_host = "http://foruda.host"
    config.foruda_token = "xxx"
    config.application_id = "xxx"
end
```

And then in your uploader, set the storage to `:foruda`

```ruby
class ModelAvatarUploader < CarrierWave::Uploader::Base
    storage :foruda

    ## or you can override configuration in uploader like this:
    self.foruda_enabled = true
    self.foruda_host = "http://foruda.host"
    self.foruda_token = "xxx"
    self.application_id = "xxx"
    self.is_private = 1
end
```

To see more details about use [Foruda](https://gitee.com/oscstudio/foruda)

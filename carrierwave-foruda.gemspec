# frozen_string_literal: true

$LOAD_PATH.push File.expand_path('lib', __dir__)

Gem::Specification.new do |s|
  s.name = 'carrierwave-foruda'
  s.version = '0.0.4'
  s.platform = Gem::Platform::RUBY
  s.authors = ['Edmondfrank']
  s.email = ['edmomdfrank@yahoo.com']
  s.homepage = 'https://gitee.com/oscstudio/carrierwave-foruda'
  s.summary = 'Foruda Storage support for CarrierWave'
  s.description = 'Foruda Storage support for CarrierWave'
  s.files = Dir.glob('lib/**/*') + %w[README.md CHANGELOG.md]
  s.require_paths = ['lib']

  s.add_dependency 'addressable', ['>= 2.5.0']
  s.add_dependency 'carrierwave', ['>= 0.11.0']
  s.add_dependency 'rest-client', ['>= 2.0.0']
end

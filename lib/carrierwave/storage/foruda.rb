# frozen_string_literal: true

require 'rest-client'

module CarrierWave
  module Storage
    class Foruda < Abstract

      class UploadError < RuntimeError; end

      def initialize(*)
        super
        @cache_called = nil
      end

      def store!(file)
        if @uploader.foruda_enabled
          f = File.new(uploader, self, uploader.store_path, uploader.application_id)
          f.store(file)
          f
        else
          path = ::File.expand_path(uploader.store_path, uploader.root)
          if uploader.move_to_store
            file.move_to(path, uploader.permissions, uploader.directory_permissions)
          else
            file.copy_to(path, uploader.permissions, uploader.directory_permissions)
          end
        end
      end

      def retrieve!(identifier)
        if identifier.start_with?('http')
          File.new(uploader, self, uploader.store_path(identifier), uploader.application_id)
        else
          path = ::File.expand_path(uploader.store_path(identifier), uploader.root)
          CarrierWave::SanitizedFile.new(path)
        end
      end

      def cache!(new_file)
        new_file.move_to(::File.expand_path(uploader.cache_path, uploader.root), uploader.permissions, uploader.directory_permissions, true)
      rescue Errno::EMLINK, Errno::ENOSPC => e
        raise(e) if @cache_called
        @cache_called = true

        # NOTE: Remove cached files older than 10 minutes
        clean_cache!(600)

        cache!(new_file)
      end

      def retrieve_from_cache!(identifier)
        CarrierWave::SanitizedFile.new(::File.expand_path(uploader.cache_path(identifier), uploader.root))
      end

      def delete_dir!(path)
        FileUtils.rm_rf(::File.expand_path(path, uploader.root)) if path
      end

      def clean_cache!(seconds)
        Dir.glob(::File.expand_path(::File.join(uploader.cache_dir, '*'), CarrierWave.root)).each do |dir|
          # generate_cache_id returns key formated TIMEINT-PID-COUNTER-RND
          time = dir.scan(/(\d+)-\d+-\d+-\d+/).first.map(&:to_i)
          time = Time.at(*time)
          if time < (Time.now.utc - seconds)
            FileUtils.rm_rf(dir)
          end
        end
      end
    end
  end
end

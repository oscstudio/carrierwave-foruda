# frozen_string_literal: true

require 'rest-client'
require 'addressable/uri'

module CarrierWave::Storage
  class Foruda
    class File
      TIMEOUT = 60 * 3 # 3 mintue

      ##
      # Returns the current path/filename of the file.
      #
      # === Returns
      #
      # [String] A path
      #

      def initialize(uploader, base, path, application_id)
        @root = uploader.store_dir
        @uploader = uploader
        @base = base
        @basename = path
        @application_id = application_id

        # Foruda files don't need consider uploader.store_dir, which is decided by Fordu service.
        real_path = @uploader.model.read_attribute(@uploader.mounted_as)
        @path = Addressable::URI.parse(real_path).path
      end

      ##
      # Returns the current path/filename of the file.
      #
      # === Returns
      #
      # [String] A path
      #

      attr_reader :path

      def escaped_path
        @escaped_path ||= CGI.escape(@path)
      end

      ##
      # Reads the contents of the file from Foruda.
      #
      # === Returns
      #
      # [String] contents of the file
      #

      def read
        res = download(path)
        res[:file].read
      rescue => ex
        puts "carrierwave-foruda read failed: #{ex.inspect}"
        nil
      end

      ##
      # Reads the heads of the file from Foruda.
      #
      # === Returns
      #
      # [Hash] headers of the file
      # - content_type: file mime value
      # - content_length: file size

      def headers
        @headers ||= head(path)
      end

      ##
      # Check the file whether exists.
      #
      # === Returns
      #
      # [Boolean]

      def exists?
        !headers.empty?
      end

      ##
      # Return the file size.
      #
      # === Returns
      #
      # [Integer]

      def size
        headers[:content_length].to_i
      end

      ##
      # Return the file mime type.
      #
      # === Returns
      #
      # [String]

      def content_type
        headers[:content_type].to_s
      end

      ##
      # Delete the file on Foruda by path.
      #
      # === Returns
      #
      # [Boolean]
      # - true: success
      # - false/nil: failed

      def delete
        remove(path)
        true
      rescue => ex
        puts "carrierwave-foruda delete failed: #{ex.inspect}"
        nil
      end

      ##
      # Return the file url.
      #
      # === Returns
      #
      # [String]

      def url
        return nil unless @uploader.foruda_host

        ::File.join(@uploader.foruda_host, @path)
      end

      ##
      # Return the file name.
      #
      # === Returns
      #
      # [String]

      def filename
        return unless url

        ::File.basename(url.split('?').first)
      end

      alias identifier path

      ##
      # Return the file extension.
      #
      # === Returns
      #
      # [String]

      def extension
        path_elements = path.split('.')
        path_elements.last if path_elements.size > 1
      end

      ##
      # === Parameters
      #
      # [new_file (Carrierwave::SanitizedFile)] the file to store
      #
      # === Returns
      #
      # [True] or Raise Error

      def store(new_file)
        res = upload(@application_id, filename, new_file.read)
        check_response!(res)
        @path = Addressable::URI.parse(res[:url]).path
        @uploader.model.update_column(@uploader.mounted_as, res[:url])
        true
      end

      ##
      # === Parameters
      #
      # [new_path (String)] the new_path for file to save
      #
      # === Returns
      #
      # [Carrierwave::Storage::Fordu::File] or Raise Error

      def copy_to(new_path)
        res = upload(@application_id, ::File.basename(new_path), read)
        check_response!(res)
        @path = Addressable::URI.parse(res[:url]).path
        @uploader.model.update_column(@uploader.mounted_as, res[:url])
        File.new(@uploader, @base, res[:url], @application_id)
      end

      private

      def check_response!(res)
        raise UploadError, res[:message] unless res[:status]
      end

      def upload(application_id, filename, content)
        res = RestClient::Request.new(
          method: :post,
          url: ::File.join(@uploader.foruda_host, 'upload'),
          headers: { Authorization: authorization },
          timeout: TIMEOUT,
          payload: {
            multipart: true,
            application_id: application_id,
            uploader_id: @uploader.model.id,
            private: @uploader.is_private.to_i,
            file_name: filename,
            file: content
          }
        ).execute
        JSON.parse(res.body)['data'].symbolize_keys.merge({ status: true })
      rescue => ex
        { status: false, message: ex.message }
      end

      def download(path)
        res = RestClient::Request.new(
          method: :get,
          url: ::File.join(@uploader.foruda_host, path),
          headers: { Authorization: authorization },
          timeout: TIMEOUT,
          raw_response: true
        ).execute
        { file: res.file, status: true }
      rescue => ex
        { file: nil, status: false, message: ex.message }
      end

      def head(path)
        res = RestClient::Request.new(
          method: :head,
          url: ::File.join(@uploader.foruda_host, path),
          headers: { Authorization: authorization },
          timeout: TIMEOUT
        ).execute
        res.headers
      rescue => ex
        {}
      end

      def remove(path)
        RestClient::Request.new(
          method: :delete,
          url: ::File.join(@uploader.foruda_host, path),
          headers: { Authorization: authorization },
          timeout: TIMEOUT
        ).execute
      end

      def authorization
        "bearer #{@uploader.foruda_token}"
      end
    end
  end
end

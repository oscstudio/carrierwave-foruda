# frozen_string_literal: true

require 'carrierwave/storage/foruda'
require 'carrierwave/storage/foruda/file'
require 'carrierwave/foruda/configuration'

CarrierWave.configure do |config|
  config.storage_engines.merge!(foruda: 'CarrierWave::Storage::Foruda')
end

CarrierWave::Uploader::Base.include CarrierWave::Foruda::Configuration
